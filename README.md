# Ansible Operator - Croot
An Ansible-based Operator, for the corresponding [Croot Role].

Minimal files to form a functional operator. In this early version
we just introduce the conventional `build` and `deploy` directories.
There should be much more for testing but, for now, this is all we need.

Our operator logic is encoded in a separate Ansible Role that's
installed from [Ansible Galaxy] during the build. This way we keep our
application logic and operator implementation separate.
 
The documentation for the [Ansible SDK] has plenty of information on
building and deploying operators, what follows is a summary of what you can
find there.  

>   For deployment instructions refer to our [chronicler] peer project,
    whose structure is identical to this project.

---

[ansible galaxy]: https://galaxy.ansible.com/matildapeak/croot
[ansible sdk]: https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/user-guide.md
[chronicler]: https://gitlab.com/matilda.peak/ansible-operator-chronicler
[croot role]: https://github.com/MatildaPeak/ansible-role-croot
